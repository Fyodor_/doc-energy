<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'docenergy');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PTP&x#ZSB8SC*kge76%NNFmc33pXd3%ab)F%7B3#X9HDWPCj6!NZWjTwAZXKC80(');
define('SECURE_AUTH_KEY',  'pOImpY3OsSkG&MMZ@equU2mk#cMkZVG)l9lqc%JRz^9%X(F6J(^cGn46(00am8Vq');
define('LOGGED_IN_KEY',    'pR!PkdT1swG5%X*h&VPOUaSC8njkaN7UYR0X%lSb7sFiLX*CseBpqjxn&VuaxO&s');
define('NONCE_KEY',        '!%*laqrwr)*yX&^EM9wtXr!lCAjClle8Epj0ZLN6He0#rPelBvs!93V#^94Oy8KB');
define('AUTH_SALT',        '^)ibWHuoM1v6L55Q39Y!8fbQXPqVocS52OVCgfKXVSC5#B0sHwagZIem5CEEbEK^');
define('SECURE_AUTH_SALT', 'YLHqL%H8&jqzm^0oo!339OmyqqZohInKt8Uf3wcQ8%iiEpehqtEjFDt9lYc^8CNL');
define('LOGGED_IN_SALT',   'i@W@i(32LMOIbCHkiS9u9&n2aMwUGTZgdeWh#0YIgLTdU@yX*vxnuRoTI1kRc2td');
define('NONCE_SALT',       'wLV5Z0#8dCS1SQ&uSc&!3EF0ieY#iQ^PmFAj17ug(Zv6aQjC9VVV%C4^wQ5hdZPB');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');

//define( 'WP_AUTO_UPDATE_CORE', minor );

?>