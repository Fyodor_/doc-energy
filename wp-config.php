<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'docenergy');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'O0^rnj7YCyRPa7mMbVQ7NAvzgXYU5Fh&5gs1LNIh(u%aajouitOtV)DkkU5EgmBj');
define('SECURE_AUTH_KEY',  'y0nF%!RF4MR^vR*@hlBH9ZhRKmgyieWiu37vII*Fm#F!874s5pHjIZ()o8l4buNl');
define('LOGGED_IN_KEY',    'Qb!GOExGZAz4NeayJz#ICJSA2)OUp3TweB4zEJlfZbV7fTNnGzta4h1FN#L^MkFb');
define('NONCE_KEY',        'hRqGISQ)4P#S8ws)jPoAcAyebaZdT*H2)Z8g8yFzKH0(0Mdr5YOKX^tPSRBl97P@');
define('AUTH_SALT',        ')*#J5(AKP(#JWzGBw%MM3VMC0^Nhe@K4yFz5F@Do5XH%TL00dQV1#6W*t(Ny@uxx');
define('SECURE_AUTH_SALT', 'YVyffWI274*qUMM8cQ9mpzQwGAjcp6Vo4lENK(IMDK1NOet#ClpIXmyWQ%(djVCf');
define('LOGGED_IN_SALT',   'xR3JLcwNa%kJYDU@5k8AlhxMWdMiZ#YtblO3mROwEQPe#MP*tKMVBci0SC3o&7h7');
define('NONCE_SALT',       '#2u1hT)RLGcX#mroNFS3US&wZtLkp3oC&PBC1es^jzYO^gX&^SllB0aX45D01j#l');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');

define( 'AUTOMATIC_UPDATER_DISABLED', true );
?>